---
title: "example plot"
output: pdf_document
date: '2022-10-07'
---

```{r setup, include=FALSE}
command = paste("bash","runner_headless.sh",sep=" ")
system(command)

data.height<-read.csv("output/height.csv")
data.growFac<-read.csv("output/growth.csv")

knitr::opts_chunk$set(echo = TRUE)
```

## Tree 1

The first tree

### Height over Time
```{r}
 plot(data.height[,1],main="hight of the tree",type = "l")
  
```

### growth factor


```{r }
 plot(data.growFac[,1],main="hight of the tree",type = "l")

```


## Tree 2

The second tree

### Height over Time
```{r}
 plot(data.height[,2],main="hight of the tree",type = "l")
  
```

### growth factor


```{r }
 plot(data.growFac[,2],main="hight of the tree",type = "l")

```

## Compare trees
### Height
```{r }
plot(data.height[,1],main="hight of the tree",type = "l")
lines(data.height[,2],col=2,lwd=2)
legend("topleft", legend=names(data.height),col=c(1:length(data.height[1,])), lty = 1:2, cex=0.8)

```
### growth factor
```{r }
plot(data.growFac[,1],main="hight of the tree",type = "l")
lines(data.growFac[,2],col=2,lwd=2)
legend("topleft", legend=names(data.growFac),col=c(1:length(data.growFac[1,])), lty = 1:2, cex=0.8)

```
